#!/usr/bin/env python3

import subprocess
import sys
from collections import namedtuple
from os import environ, path
from typing import List, Tuple

from mypy import api

class CheckError(Exception):
    def __init__(self, message):
        self.message = message

BColors = namedtuple('BColors', ['HEADER', 'OK_BLUE', 'OK_GREEN', 'WARNING',
                                 'FAIL', 'ENDC', 'BOLD', 'UNDERLINE'])
BCOLORS = BColors(HEADER='\033[95m',
                  OK_BLUE='\033[94m',
                  OK_GREEN='\033[92m',
                  WARNING='\033[93m',
                  FAIL='\033[91m',
                  ENDC='\033[0m',
                  BOLD='\033[1m',
                  UNDERLINE='\033[4m')

PREFIX: str = environ.get('MESON_SOURCE_ROOT')
SRCFILES: List[str] = [
  'main.py',
  'application.py',
  'window.py',
  'cachedfile.py',
  'iconview.py',
  'listview.py',
  'viewcontroller.py',
  'containermaxwidth.py',
  'workerpool.py',
  'iconinfo.py',
  'settings.py',
  'pythonutil.py',
  'singleton.py',
  'pathbar.py',
  'sidebar.py',
]

def run_type_analizer() -> None:
    print(BCOLORS.HEADER + '############################################'+ BCOLORS.ENDC)
    print(BCOLORS.HEADER + '    Running type analysis with Mypy...      ' + BCOLORS.ENDC)
    print(BCOLORS.HEADER + '############################################'+ BCOLORS.ENDC)
    types_are_good: bool = True
    for srcfile in SRCFILES:
        result: Tuple[str, str, int] = api.run([path.join(PREFIX, 'hickel', srcfile),
                                                '--follow-imports=skip',
                                                '--ignore-missing-imports',
                                                # Gtk.FlowBox and such
                                                '--allow-subclassing-any',
                                                # Gtk.Callback and such
                                                '--allow-untyped-decorators',
                                                '--strict'])
        if result[0] != "":
            print(BCOLORS.FAIL + 'Type checking has the following errors' + BCOLORS.ENDC)
            print(result[0])
            types_are_good = False
        if result[1] != "":
            print(BCOLORS.FAIL + 'Type checking has failed' + BCOLORS.ENDC)
            print(result[1])
            types_are_good = False

    if types_are_good:
        print(f'{BCOLORS.OK_GREEN}############################################{BCOLORS.ENDC}')
        print(f'{BCOLORS.OK_GREEN}           🌱 Types are all good 🌱        {BCOLORS.ENDC}')
        print(f'{BCOLORS.OK_GREEN}############################################{BCOLORS.ENDC}')
    else:
        raise CheckError('Type checking failed')

def run_safe_code_analizer() -> None:
    print(BCOLORS.HEADER + '################################################'+ BCOLORS.ENDC)
    print(BCOLORS.HEADER + '#### Running safety analysis with Pylint... ####' + BCOLORS.ENDC)
    print(BCOLORS.HEADER + '################################################'+ BCOLORS.ENDC)
    types_are_good: bool = True
    for srcfile in SRCFILES:
        # We want to do a pass of only assertive errors
        # Disables checks for:
        #   - E: Style conventions
        #   - R: Refactor conventions
        #   - W: Warnings for possible errors
        result = subprocess.run(['pylint', '--disable', 'R,C,W',
                                 path.join(PREFIX, 'hickel', srcfile)],
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        decoded_stdout = result.stdout.decode('utf-8')
        decoded_stderr = result.stderr.decode('utf-8')
        if "Your code has been rated at 10.00/10" not in decoded_stdout:
            print(BCOLORS.FAIL + 'Code safety has the following errors:' + BCOLORS.ENDC)
            print(decoded_stdout)
            types_are_good = False
        if decoded_stderr != "":
            print(BCOLORS.FAIL + 'Code safety checking has failed' + BCOLORS.ENDC)
            print(decoded_stderr)
            types_are_good = False

    if types_are_good:
        print(BCOLORS.OK_GREEN + '🍹 Code safety is all good 🍹\n' + BCOLORS.ENDC)
    else:
        raise CheckError('Type checking failed')

def run_style_analizer_pylint() -> None:
    print(BCOLORS.HEADER + '###############################################'+ BCOLORS.ENDC)
    print(BCOLORS.HEADER + '#### Running style analysis with Pylint... ####' + BCOLORS.ENDC)
    print(BCOLORS.HEADER + '###############################################'+ BCOLORS.ENDC)
    style_is_good: bool = True
    for srcfile in SRCFILES:
        # We want to do a pass of things that are not errors
        # Disables checks for:
        #   - E: Errors for safe code
        #   - C0111: Missing docstrings
        #   - C0413: Imports should be placed at the top. This is need for
        #            importing a specific version of gi
        #   - W0613: Unused arguments in function body. This is pretty common in
        #            callbacks, not all arguments are used.
        #   - W0221: Arguments differ. Seems pygobject removes the 'self'
        #            argument from the signature, and that confuses the static
        #            checker. See https://gitlab.gnome.org/GNOME/pygobject/issues/231
        #   - W0511: TODO and FIXME items. We don't want them to raise a warning.
        #   - R0903: Too few methods for a class. We want to use data classes,
        #            but seems that's frowed upon so far until the PEP for
        #            @dataclass is implemented. See https://www.python.org/dev/peps/pep-0557/#id17
        result = subprocess.run(['pylint', '--disable', 'E,C0111,C0413,W0613,W0221,W0511,R0903',
                                 path.join(PREFIX, 'hickel', srcfile)],
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        decoded_stdout = result.stdout.decode('utf-8')
        decoded_stderr = result.stderr.decode('utf-8')
        if "Your code has been rated at 10.00/10" not in decoded_stdout:
            print(BCOLORS.FAIL + 'Code style has the following errors:' + BCOLORS.ENDC)
            print(decoded_stdout)
            style_is_good = False
        if decoded_stderr != "":
            print(BCOLORS.FAIL + 'Code style checking has failed' + BCOLORS.ENDC)
            print(decoded_stderr)
            style_is_good = False

    if style_is_good:
        print(BCOLORS.OK_GREEN + '✨ Code style is all good ✨\n ' + BCOLORS.ENDC)

def run_error_analizer_flake8() -> None:
    print(BCOLORS.HEADER + '###############################################'+ BCOLORS.ENDC)
    print(BCOLORS.HEADER + '#### Running error analysis with Flake8... ####' + BCOLORS.ENDC)
    print(BCOLORS.HEADER + '###############################################'+ BCOLORS.ENDC)

    style_is_good: bool = True
    for srcfile in SRCFILES:
        result = subprocess.run(['flake8', '--select', 'F', '--show-source',
                                 path.join(PREFIX, 'hickel', srcfile)],
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        decoded_stdout = result.stdout.decode('utf-8')
        decoded_stderr = result.stderr.decode('utf-8')
        if decoded_stdout != "":
            print(BCOLORS.FAIL + 'Code error analysis has found the following errors:' + BCOLORS.ENDC)
            print(decoded_stdout)
            style_is_good = False
        if decoded_stderr != "":
            print(BCOLORS.FAIL + 'Code error analysis has failed' + BCOLORS.ENDC)
            print(decoded_stderr)
            style_is_good = False

    if style_is_good:
        print(BCOLORS.OK_GREEN + '🌱 Code error analysis is all good 🌱\n ' + BCOLORS.ENDC)


def run_style_analizer_flake8() -> None:
    print(BCOLORS.HEADER + '###############################################'+ BCOLORS.ENDC)
    print(BCOLORS.HEADER + '#### Running style analysis with Flake8... ####' + BCOLORS.ENDC)
    print(BCOLORS.HEADER + '###############################################'+ BCOLORS.ENDC)

    style_is_good: bool = True
    for srcfile in SRCFILES:
        # Ignores:
        #   - E402: Having only imports at the top, neccessary for requiring a
        #           gi specific version
        #   - W503: Break new line after operands. PEP8 has changed its mind and now
        #           recomends to break before the operands. Flake8 just needs to catch
        #           up with that.
        result = subprocess.run(['flake8', '--ignore', 'E402,W503', '--show-source',
                                 path.join(PREFIX, 'hickel', srcfile)],
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        decoded_stdout = result.stdout.decode('utf-8')
        decoded_stderr = result.stderr.decode('utf-8')
        if decoded_stdout != "":
            print(BCOLORS.FAIL + 'Code style has the following errors:' + BCOLORS.ENDC)
            print(decoded_stdout)
            style_is_good = False
        if decoded_stderr != "":
            print(BCOLORS.FAIL + 'Code style checking has failed' + BCOLORS.ENDC)
            print(decoded_stderr)
            style_is_good = False

    if style_is_good:
        print(BCOLORS.OK_GREEN + '✨ Code style is all good ✨\n ' + BCOLORS.ENDC)

print('\n')
if __name__ == "__main__":
    import os
    # Change current working path to the this file parent's, then change to the
    # root folder. This is done so imports checking work properly, as they would
    # be imported once installed
    os.chdir(os.path.dirname(__file__))
    os.chdir('../../')

    # Pylint is really slow
    # run_safe_code_analizer()
    # run_style_analizer_pylint()

    # run_style_analizer_flake8()
    # run_error_analizer_flake8()
    run_type_analizer()
