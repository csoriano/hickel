# pathbar.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Tuple, Optional

import gi
from gi.repository import Gtk, Gdk, GObject, Gio, GLib, Pango


class PathBarOverflowContainer(Gtk.Box):

    def __init__(self) -> None:
        super().__init__()
        self.connect("size-allocate", self._on_size_allocate)

    @staticmethod
    def do_get_request_mode() -> Gtk.SizeRequestMode:
        return Gtk.SizeRequestMode.WIDTH_FOR_HEIGHT

    def do_measure(self, orientation: Gtk.Orientation, for_size: int) -> Tuple[int, int, int, int]:
        children = self.get_children()
        minimum, natural = 0, 0
        if children:
            minimum, natural, _skip, _skip = children[-1].measure(orientation, for_size)

        return minimum, natural, -1, -1

    def _on_size_allocate(self, _widget: Gtk.Widget, _allocation: Gdk.Rectangle, _baseline: int) -> None:
        GLib.idle_add(self._update_children_visibility)

    def _update_children_visibility(self) -> None:
        cum_min_width = 0
        cum_nat_width = 0
        allocation = self.get_allocation()
        children = self.get_children()
        for child in reversed(children):
            button = child.get_child()
            min_width, nat_width, _skip, _skip = button.measure(Gtk.Orientation.HORIZONTAL,
                                                                allocation.height)
            cum_min_width += min_width
            cum_nat_width += nat_width
            if cum_min_width > allocation.width:
                child.set_reveal_child(False)
            else:
                child.set_reveal_child(True)


class PathBar(GObject.GObject):
    __gtype_name__ = 'PathBar'

    def __init__(self) -> None:
        super().__init__()
        self._overflow_container = PathBarOverflowContainer()
        self._overflow_container.set_hexpand(True)
        self._location: Optional[Gio.File] = None
        style = self._overflow_container.get_style_context()
        style.add_class('path-bar')

    @GObject.Property(type=Gio.File)
    def location(self) -> Gio.File:
        return self._location

    @location.setter  # type: ignore
    def location(self, value) -> None:
        self._location = value
        self._update_path_bar()

    @GObject.Property(type=Gtk.Widget)
    def widget(self) -> Gtk.Widget:
        return self._overflow_container

    def _update_path_bar(self) -> None:
        for child in self._overflow_container.get_children():
            child.destroy()

        if not self._location:
            return

        if not self._location.has_uri_scheme('file'):
            scheme = self._location.get_uri_scheme()
            self._overflow_container.add(self._create_button_ui(scheme))

        path = self._location.get_path()
        if path is None:
            return

        path_parts = path.split(GLib.DIR_SEPARATOR_S)
        # The first slash
        del path_parts[0]
        for file_name in path_parts:
            button = self._create_button_ui(file_name)
            self._overflow_container.add(button)

    @staticmethod
    def _create_button_ui(file_name: str) -> Gtk.Widget:
        revealer = Gtk.Revealer.new()
        box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 6)
        label = Gtk.Label.new(file_name)
        label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
        button = Gtk.Button.new()
        button.add(label)
        style_context: Gtk.StyleContext = button.get_style_context()
        style_context.add_class("flat")
        slash = Gtk.Label.new(GLib.DIR_SEPARATOR_S)
        box.add(slash)
        box.add(button)
        revealer.add(box)
        revealer.set_transition_type(Gtk.RevealerTransitionType.SLIDE_LEFT)

        return revealer
