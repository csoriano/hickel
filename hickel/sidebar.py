# sidebar.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Optional

import gi
from gettext import gettext as _
from gi.repository import Gtk, Gio, GLib, GObject

class Sidebar(Gtk.Bin):

    def __init__(self) -> None:
        super().__init__()
        self._location: Optional[Gio.File] = None
        self.set_size_request(180, -1)
        self._volume_monitor = Gio.VolumeMonitor.get()
        style_context = self.get_style_context()
        style_context.add_class('sidebar')

        self._list = Gtk.ListBox()
        self._list.connect('row-activated',
                           lambda _, row: setattr(self, 'location', row.location))
        self._list.set_hexpand(True)
        self._list.set_vexpand(True)
        self.add(self._list)
        self._add_places()

        self.location: Gio.File = Gio.File.new_for_commandline_arg(GLib.get_home_dir())

    def _add_places(self) -> None:
        self._list.add(SidebarRow(_('Recent'), Gio.File.new_for_uri('recent:///'),
                                  Gio.ThemedIcon.new_with_default_fallbacks("document-open-recent-symbolic")))

        self._add_network_places()

        self._list.add(SidebarRow(_('Starred'), Gio.File.new_for_uri('starred:///'),
                                  Gio.ThemedIcon.new_with_default_fallbacks("starred-symbolic")))
        home_dir = Gio.File.new_for_commandline_arg(GLib.get_home_dir())
        self._list.add(SidebarRow(_('This Device'), home_dir,
                                  Gio.ThemedIcon.new_with_default_fallbacks("computer-symbolic")))
        downloads_dir = Gio.File.new_for_commandline_arg(GLib.get_home_dir() + '/Downloads')
        self._list.add(SidebarRow(_('Downloads'), downloads_dir,
                                  Gio.ThemedIcon.new_with_default_fallbacks("folder-download-symbolic")))
        self._list.add(SidebarRow(_('Trash'), Gio.File.new_for_uri('trash:///'),
                                  Gio.ThemedIcon.new_with_default_fallbacks("user-trash-symbolic")))

    def _add_network_places(self) -> None:
        network_volumes = []
        network_mounts = []
        for volume in self._volume_monitor.get_volumes():
            drive = volume.get_drive()
            if not drive:
                continue
            identifier = volume.get_identifier('class')
            if identifier == 'network':
                network_volumes.append(volume)

        for mount in self._volume_monitor.get_mounts():
            if mount.is_shadowed():
                continue
            volume = mount.get_volume()
            if volume:
                continue
            root = mount.get_default_location()
            if not root.is_native():
                network_mounts.append(mount)
                continue
            # FIXME: not using volume to mount yet
            self._list.add(SidebarRow(mount.get_name(), Gio.File.new_for_uri('file:///'),
                                      mount.get_symbolic_icon()))

        for volume in network_volumes:
            mount = volume.get_mount()
            if not mount:
                network_mounts.append(mount)
                continue
            self._list.add(SidebarRow(volume.get_name(), root,
                                      volume.get_symbolic_icon()))
        for mount in network_mounts:
            self._list.add(SidebarRow(mount.get_name(), mount.get_default_location(),
                                      mount.get_symbolic_icon()))


    @GObject.Property(type=Gio.File)
    def location(self) -> Optional[Gio.File]:
        return self._location

    @location.setter # type: ignore
    def location(self, value: Gio.File) -> None:
        self._location = value
        self._list.unselect_all()
        for row in self._list.get_children():
            if row.location.equal(self._location):
                self._list.select_row(row)
                break


class SidebarRow(Gtk.ListBoxRow):

    def __init__(self, name: str, location: Gio.File, icon: Gio.Icon) -> None:
        super().__init__()
        self._name = name
        self._location = location
        self._icon = icon
        style_context = self.get_style_context()
        style_context.add_class('sidebar-row')

        self._create_ui()

    def _create_ui(self) -> None:
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL,
                      spacing=8)
        image = Gtk.Image()
        image.set_from_gicon(self._icon)
        box.add(image)
        box.add(Gtk.Label(label=self._name, vexpand=True))
        self.add(box)


    @GObject.Property(type=Gio.File)
    def location(self) -> Gio.File:
        return self._location

    @location.setter # type: ignore
    def location(self, value: Gio.File) -> None:
        self._location = value
