# cachedfile.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject, Gio
from typing import Optional

class CachedFile(GObject.GObject):

    def __init__(self) -> None:
        GObject.GObject.__init__(self)
        self._location: Optional[Gio.File] = None
        self._display_name: str = ""
        self._icon: Optional[Gio.Icon] = None
        self._modification_time: str = ""
        self._size: str = ""
        self._hidden: bool = False
        self._content_type: str = ""
        self._is_directory: bool = False

    @GObject.Property(type=str)
    def display_name(self) -> str:
        return self._display_name

    @display_name.setter # type: ignore
    def display_name(self, value) -> None:
        self._display_name = value

    @GObject.Property(type=bool, default=False)
    def hidden(self) -> bool:
        return self._hidden

    @hidden.setter # type: ignore
    def hidden(self, value) -> None:
        self._hidden = value

    @GObject.Property(type=Gio.Icon)
    def icon(self) -> Gio.Icon:
        return self._icon

    @icon.setter # type: ignore
    def icon(self, value) -> None:
        self._icon = value

    @GObject.Property(type=str)
    def modification_time(self) -> str:
        return self._modification_time

    @modification_time.setter # type: ignore
    def modification_time(self, value) -> None:
        self._modification_time = value

    @GObject.Property(type=str)
    def size(self) -> str:
        return self._size

    @size.setter # type: ignore
    def size(self, value) -> None:
        self._size = value

    @GObject.Property(type=Gio.File)
    def location(self) -> Gio.File:
        return self._location

    @location.setter # type: ignore
    def location(self, value: Gio.File) -> None:
        self._location = value

    @GObject.Property(type=str)
    def content_type(self) -> str:
        return self._content_type

    @content_type.setter # type: ignore
    def content_type(self, value) -> None:
        self._content_type = value

    @GObject.Property(type=bool, default=False)
    def is_directory(self) -> str:
        return self._is_directory

    @is_directory.setter # type: ignore
    def is_directory(self, value) -> None:
        self._is_directory = value
