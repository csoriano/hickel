# listview2.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from gi.repository import Gtk, GObject

from hickel.cachedfile import CachedFile
from hickel.viewcontroller import ViewController
from hickel import iconinfo


@Gtk.Template.from_resource('/org/gnome/Hickel/listview2.ui')
class ListView2(Gtk.Box):
    __gtype_name__='ListView2'

    list_box: Gtk.Button = Gtk.Template.Child('list_box')

    name_header: Gtk.Button = Gtk.Template.Child('name_header')
    size_header: Gtk.Button = Gtk.Template.Child('size_header')
    modified_header: Gtk.Button = Gtk.Template.Child('modified_header')

    name_size_group: Gtk.Button = Gtk.Template.Child('name_size_group')
    size_size_group: Gtk.Button = Gtk.Template.Child('size_size_group')
    modified_size_group: Gtk.Button = Gtk.Template.Child('modified_size_group')

    def __init__(self, controller: ViewController) -> None:
        super().__init__()
        self._controller = controller
        self._model = self._controller.model
        self.list_box.bind_model(self._model, self._create_widget, None)

    def _create_widget(self, item: CachedFile, _user_data: None) -> Gtk.Widget:
        row = ListRow(self.name_size_group, self.size_size_group, self.modified_size_group)
        row.cached_file = item

        return row

class ListRow(Gtk.ListBoxRow):

    def __init__(self, name_size_group, size_size_group, modified_size_group):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_from_resource('/org/gnome/Hickel/listviewrow.ui')
        self._container = builder.get_object('row')
        self._name_column = builder.get_object('name_column')
        self._file_icon = builder.get_object('file_icon')
        self._name_label = builder.get_object('file_name_label')
        self._name_size_separator = builder.get_object('name_size_separator')
        self._size_label = builder.get_object('size_label')
        self._size_modified_separator = builder.get_object('size_modified_separator')
        self._modified_label = builder.get_object('modified_label')

        name_size_group.add_widget(self._name_column)
        size_size_group.add_widget(self._size_label)
        modified_size_group.add_widget(self._modified_label)

        self.add(self._container)

        self._cached_file = None

    @GObject.Property(type=CachedFile)
    def cached_file(self) -> CachedFile:
        return self._cached_file

    @cached_file.setter # type: ignore
    def cached_file(self, value: CachedFile) -> None:
        self._cached_file = value
        # We cannot fo to the regular background task way, since this needs to be handled by
        # gtk+ itself. load_icon_sync is not thread safe, as nothing in gtk+ is.
        self._name_label.set_text(self._cached_file.display_name)
        iconinfo.load_icon(self._cached_file, self._name_column,
                           self._file_icon, iconinfo.LIST_ICON_SIZE)
        self._size_label.set_text(self._cached_file.size)
        self._modified_label.set_text(self._cached_file.modification_time)
