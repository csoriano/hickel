# containermaxwidth.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations
from typing import Optional, Tuple, List
from gi.repository import Gtk, GObject

class ContainerMaxWidth(Gtk.Bin):
    __gtype_name__ = 'ContainerMaxWidth'

    DEFAULT_MAX_SIZE = 120

    def __init__(self) -> None:
        super().__init__()
        self.set_halign(Gtk.Align.CENTER)
        self._max_width: Optional[int] = None

    @GObject.Property(type=int)
    def max_width(self) -> int:
        return self._max_width

    @max_width.setter # type: ignore
    def max_width(self, value: int) -> None:
        self._max_width = value

    def do_measure(self, orientation: Gtk.Orientation,
                   for_size: int) -> Tuple[int, int, int, int]:
        child = self.get_child()
        if not child:
            return -1, -1, -1, -1

        minimum, natural, _x, _x = child.measure(orientation, for_size)
        if self._max_width is not None and orientation == Gtk.Orientation.HORIZONTAL:
            natural = min(self._max_width, natural)

        return minimum, natural, -1, -1
