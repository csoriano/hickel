# main.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import sys
import traceback
from typing import Any, Dict
import logging

import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk
from hickel.application import Application
from hickel.settings import SettingsManager
from hickel.pythonutil import ProfileFilter

def on_unhandled_exception(_exception_type: BaseException,
                           _value: BaseException, _trace_back: Any) -> None:
    traceback.print_last()
    sys.exit(-1)

def options() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="A prototype of a file manager similar to \
                                                 Nautilus for fast prototyping products")
    parser.add_argument('--log_level', type=str, default='INFO',
                        help="log in a hickel.log file")
    parser.add_argument('--profile', action='store_true',
                        help="Profile and log critical functions")

    return parser.parse_args()

def main(_version: int, **_kwargs: Any) -> int:
    # Exit the application on unhandled exception.
    # PyGobject was just printing it for some reason, makes things hard to debug.
    sys.excepthook = on_unhandled_exception # type: ignore
    cli_args = options()
    SettingsManager().profile = cli_args.profile
    numeric_level = getattr(logging, cli_args.log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % cli_args.log_level)
    logging.basicConfig(level=cli_args.log_level, filename='hickel.log', filemode='w',
                        format='%(levelname) s%(relativeCreated)6d %(threadName)s %(module)s.%(funcName)s %(message)s')
    logger = logging.getLogger()
    logger.addFilter(ProfileFilter())

    app: Gtk.Application = Application()
    return int(app.run(sys.argv))
