# workerpool.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import unittest
import logging
from concurrent.futures import ThreadPoolExecutor, Future
from enum import IntEnum, auto
from multiprocessing import cpu_count
from threading import Event
from typing import Callable, Any, Optional, Dict, Union, List

import gi
gi.require_version("Gdk", "4.0")
from gi.repository import GLib, Gdk

from hickel.singleton import Singleton


class Priority(IntEnum):
    HIGH = auto()
    BACKGROUND = auto()


class WorkerPool(metaclass=Singleton):

    def __init__(self) -> None:
        self._pools: Dict[Priority, ThreadPoolExecutor] = {}
        self._tasks: List[Future] = []
        self._priority_mapping = {
            Priority.HIGH: GLib.PRIORITY_DEFAULT,
            Priority.BACKGROUND: GLib.PRIORITY_LOW,
        }

    def _get_pool(self, priority: Priority) -> ThreadPoolExecutor:
        """Return a (shared) pool for a given priority"""
        if priority not in self._pools:
            try:
                cpus = cpu_count()
            except NotImplementedError:
                cpus = 2
            max_workers = int(cpus * 1.5)
            self._pools[priority] = ThreadPoolExecutor(max_workers)
        return self._pools[priority]

    @staticmethod
    def _wrap_function(op: Callable, op_data: Optional[Any],
                       cancellable: Optional[Event]) -> Callable:

        def wrap() -> Any:
            if cancellable is not None and cancellable.is_set():
                return None
            return op(op_data=op_data, cancellable=cancellable)

        return wrap

    def _wrap_callback(self, priority: Priority,
                       op_data: Optional[Any] = None,
                       cancellable: Optional[Event] = None,
                       callback: Optional[Callable] = None) -> Callable:

        def callback_main(future: Future) -> bool:
            if cancellable is not None and cancellable.is_set():
                return False

            if callback is not None:
                callback(op_data, future, cancellable=cancellable)
            return False

        def callback_thread(future: Future) -> None:
            if cancellable is not None and cancellable.is_set():
                return

            glib_priority = self._priority_mapping[priority]
            GLib.idle_add(callback_main, future, priority=glib_priority)

        return callback_thread

    def _call_async(self, priority: Priority, op: Callable,
                    op_data: Optional[Any] = None,
                    cancellable: Optional[Event] = None,
                    callback: Optional[Callable] = None) -> None:
        pool = self._get_pool(priority)
        wrapped_func = self._wrap_function(op, op_data, cancellable)
        wrapped_callback = self._wrap_callback(priority, op_data, cancellable, callback)
        future = pool.submit(wrapped_func)
        future.add_done_callback(wrapped_callback)

    def terminate_all(self) -> None:
        """
        Terminate all pools, doesn't wait for task completion.
        Can be called multiple times and call_async() etc. can still be used.
        """

        for key, pool in list(self._pools.items()):
            del self._pools[key]
            pool.shutdown(wait=False)


    def add_job(self, op: Callable, op_data: Optional[Any] = None,
                cancellable: Optional[Event] = None,
                callback: Optional[Callable] = None) -> None:

        return self._call_async(Priority.HIGH, op, op_data,
                                cancellable, callback)

    def add_job_background(self, op: Callable, op_data: Optional[Any] = None,
                           cancellable: Optional[Event] = None,
                           callback: Optional[Callable] = None) -> None:

        self._call_async(Priority.BACKGROUND, op, op_data,
                         cancellable, callback)


######################################
#           Unit testing             #
######################################

class TestWorkerPool(unittest.TestCase):

    def test_basic(self) -> None:
        executor = WorkerPool()
        mainloop = GLib.MainLoop()
        executor.add_job(self.test_op, op_data=mainloop,
                         callback=self.cb)
        mainloop.run()

    @unittest.skip("Not a test")
    def test_op(self, **kwargs: Any) -> None:
        pass

    @unittest.skip("Not a test")
    def cb(self, data: GLib.Mainloop, _result: Optional[Any],
           _future: Future, **_kwargs: Any) -> None:
        data.quit()

    def test_op_exception(self) -> None:
        executor = WorkerPool()
        mainloop = GLib.MainLoop()
        executor.add_job(self.test_op_exception, op_data=mainloop,
                         callback=self.cb_with_exception_handling)
        mainloop.run()

    @unittest.skip("Not a test")
    def test_op_exception(self, **_kwargs: Any) -> None:
        print("test op exception")
        raise Exception("Op excention")

    @unittest.skip("Not a test")
    def cb_with_exception_handling(self, data: GLib.Mainloop,
                                   future: Future, **_kwargs: Any) -> None:
        data.quit()
        with self.assertRaises(Exception):
            future.result()

    def test_callback_exception(self) -> None:
        executor = WorkerPool()
        mainloop = GLib.MainLoop()
        executor.add_job(self.test_op, op_data=mainloop,
                         callback=self.cb_exception)
        mainloop.run()

    @unittest.skip("Not a test")
    def cb_exception(self, data: GLib.Mainloop,
                     _future: Future, **_kwargs: Any) -> None:
        data.quit()
        with self.assertRaises(Exception):
            raise Exception("Callback exception")
