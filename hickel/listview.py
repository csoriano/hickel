# listview.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from gi.repository import Gtk, Pango

from hickel.cachedfile import CachedFile
from hickel.viewcontroller import ViewController
from hickel import iconinfo


@Gtk.Template.from_resource('/org/gnome/Hickel/listview.ui')
class ListView(Gtk.Paned):
    __gtype_name__='ListView'
    name_list_box: Gtk.ListBox = Gtk.Template.Child('name_list_box')
    size_list_box: Gtk.ListBox = Gtk.Template.Child('size_list_box')
    modification_date_list_box: Gtk.ListBox = Gtk.Template.Child('modification_date_list_box')

    def __init__(self, controller: ViewController) -> None:
        super().__init__()
        self._controller = controller
        self._model = self._controller.model
        self.name_list_box.bind_model(self._model, self._create_widget_name, None)
        self.modification_date_list_box.bind_model(self._model,
                                                   self._create_widget_modification_date, None)
        self.size_list_box.bind_model(self._model, self._create_widget_size, None)

    @staticmethod
    def _create_widget_name(item: CachedFile, _user_data: None) -> Gtk.Widget:
        box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        box.set_size_request(-1, iconinfo.LIST_ICON_SIZE)

        placeholder = Gtk.Label.new(iconinfo.PLACEHOLDER_TEXT)
        box.pack_start(placeholder)

        label = Gtk.Label.new(item.display_name)
        label.set_hexpand(True)
        label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
        label.set_justify(Gtk.Justification.LEFT)
        label.set_halign(Gtk.Align.START)
        box.pack_end(label)

        iconinfo.load_icon(item, box, placeholder, iconinfo.LIST_ICON_SIZE)

        return box

    @staticmethod
    def _create_widget_size(item: CachedFile, _user_data: None) -> Gtk.Widget:
        box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        box.set_size_request(-1, iconinfo.LIST_ICON_SIZE)

        label = Gtk.Label.new(item.size)
        label.set_hexpand(True)
        label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
        label.set_justify(Gtk.Justification.LEFT)
        label.set_halign(Gtk.Align.START)

        box.add(label)

        return box

    @staticmethod
    def _create_widget_modification_date(item: CachedFile, _user_data: None) -> Gtk.Widget:
        box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
        box.set_size_request(-1, iconinfo.LIST_ICON_SIZE)

        label = Gtk.Label.new(item.modification_time)
        label.set_hexpand(True)
        label.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
        label.set_justify(Gtk.Justification.LEFT)
        label.set_halign(Gtk.Align.START)
        label.set_margin_start(6)

        box.add(label)

        return box
