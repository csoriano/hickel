# viewcontroller.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import List, Any, cast, Optional, Dict
import os
import time
import logging
import threading
from concurrent.futures import Future

from gi.repository import Gio, GObject, GLib

from hickel.cachedfile import CachedFile
from hickel.workerpool import WorkerPool
import hickel.pythonutil


class ViewController(GObject.GObject):
    ATTRIBUTES: str = 'standard::*,access::*,mountable::*,time::*,unix::*,owner::*,\
                       selinux::*,thumbnail::*,id::filesystem,trash::orig-path,\
                       trash::deletion-date,metadata::*,recent::*'

    def __init__(self, location: Gio.File) -> None:
        super().__init__()
        self._location = location
        self._model_lock = threading.Lock()
        self._model = Gio.ListStore.new(CachedFile)
        self._loading_cancellable: Optional[threading.Event] = None

        self._load_location()

    @GObject.Property(type=Gio.ListStore)
    def model(self) -> Gio.ListStore:
        return self._model

    @GObject.Property(type=Gio.File)
    def location(self) -> Gio.File:
        return self._location

    @location.setter  # type: ignore
    def location(self, value) -> None:
        self._location = value
        self._load_location()

    def _enumerate_files(self, **kwargs: Any) -> List[CachedFile]:
        cancellable: threading.Event = cast(threading.Event, kwargs['cancellable'])
        enumerator = self._location.enumerate_children(ViewController.ATTRIBUTES,
                                                       Gio.FileQueryInfoFlags.NONE,
                                                       None)
        files: List[CachedFile] = []
        file_info: Gio.FileInfo = enumerator.next_file()
        while file_info is not None and not cancellable.is_set():
            cached_file = CachedFile()
            cached_file.display_name = file_info.get_display_name()
            cached_file.size = file_info.get_size()
            cached_file.modification_time = file_info.get_modification_time().to_iso8601()
            cached_file.icon = file_info.get_icon()
            cached_file.name = file_info.get_name()
            cached_file.hidden = file_info.get_is_hidden()
            cached_file.is_directory = file_info.get_file_type() == Gio.FileType.DIRECTORY
            cached_file.content_type = file_info.get_content_type()
            uri = os.path.join(self._location.get_uri(), cached_file.name)
            cached_file.location = Gio.File.new_for_uri(uri)

            files.append(cached_file)
            file_info = enumerator.next_file()

        return files

    def _load_location(self) -> None:
        if self._loading_cancellable is not None:
            self._loading_cancellable.is_set()
        self._loading_cancellable = threading.Event()
        self._remove_all_files()
        worker_pool = WorkerPool()
        worker_pool.add_job(self._enumerate_files, None, self._loading_cancellable,
                            self._enumerate_files_on_loaded)

    @staticmethod
    def _compare_by_name(item_a: CachedFile, item_b: CachedFile, *_user_data: Any) -> int:
        return cast(int, GLib.strcmp0(item_a.display_name, item_b.display_name))

    def _enumerate_files_on_loaded(self, _op_data: Optional[Any],
                                   future: Future, **kwargs: Any) -> None:
        cancellable: threading.Event = kwargs['cancellable']
        if cancellable.is_set():
            return

        files: List[CachedFile] = future.result()
        filtered = [f for f in files if not f.hidden]
        self._loading_files_future = None

        # Log performance
        start = time.perf_counter_ns()

        self._model.splice(self._model.get_n_items(), 0, filtered)

        middle = time.perf_counter_ns()
        logging.info(f"Splice {(middle-start)/1000_000}",
                     extra={hickel.pythonutil.LOG_TYPE_PROFILE: True})

        self._model.sort(self._compare_by_name)

        end = time.perf_counter_ns()
        logging.info(f"Sort {(end-middle)/1000_000}")

    def _remove_all_files(self) -> None:
        self._model.remove_all()
