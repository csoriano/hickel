# iconinfo.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from dataclasses import dataclass

from gi.repository import Gtk, Gio

from hickel.workerpool import WorkerPool
from hickel.cachedfile import CachedFile

VIEW_ICON_SIZE = 96
LIST_ICON_SIZE = 48
PLACEHOLDER_TEXT = '...'

@dataclass
class LoadIconData:
    file_info: Gio.File
    container: Gtk.Container
    old_icon: Gtk.Widget
    size: int

def _load_icon_on_done(icon_info: Gtk.IconInfo, result: Gio.AsyncResult,
                       load_icon_data: LoadIconData) -> None:
    pixbuf = icon_info.load_icon_finish(result)
    container = load_icon_data.container
    old_icon = load_icon_data.old_icon

    container.remove(old_icon)
    image = Gtk.Image.new_from_pixbuf(pixbuf)
    image.set_pixel_size(load_icon_data.size)
    container.pack_start(image)

def load_icon(item: CachedFile, box: Gtk.Box,
              old_icon: Gtk.Widget, size: int) -> None:
    icon = item.icon
    load_icon_data = LoadIconData(item, box, old_icon, size)
    icon_theme = Gtk.IconTheme.get_default()
    icon_info = icon_theme.lookup_by_gicon(icon, size,
                                           Gtk.IconLookupFlags.FORCE_SIZE)
    if icon_info is None:
        icon_info = icon_theme.lookup_by_gicon(Gio.Icon.new_for_string('text-x-generic'),
                                               size, Gtk.IconLookupFlags.FORCE_SIZE)
    icon_info.load_icon_async(None, _load_icon_on_done, load_icon_data)
