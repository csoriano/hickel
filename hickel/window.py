# window.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from dataclasses import dataclass
from typing import Dict, Any, List

from gi.repository import Gtk, Gio, GLib

from hickel.cachedfile import CachedFile
from hickel.iconview import IconView
from hickel.listview2 import ListView2
from hickel.viewcontroller import ViewController
from hickel.pathbar import PathBar
from hickel.sidebar import Sidebar

DEFAULT_PATH = GLib.get_home_dir()

@dataclass
class EnumerationData:
    def __init__(self) -> None:
        self.finished: bool = False
        self.files: List[CachedFile] = []


@Gtk.Template.from_resource('/org/gnome/Hickel/window.ui')
class Window(Gtk.ApplicationWindow):
    __gtype_name__ = 'Window'

    main_paned: Gtk.Paned = Gtk.Template.Child('main_paned')
    main_stack: Gtk.Stack = Gtk.Template.Child('main_view_stack')
    view_icon_container: Gtk.ScrolledWindow = Gtk.Template.Child('view_icon_container')
    view_list_container: Gtk.ScrolledWindow = Gtk.Template.Child('view_list_container')
    view_switcher_button: Gtk.Button = Gtk.Template.Child('view_switcher_button')
    status_revealer: Gtk.Revealer = Gtk.Template.Child('status_revealer')
    status_label: Gtk.Label = Gtk.Template.Child('status_label')
    path_bar_container: Gtk.Box = Gtk.Template.Child('path_bar_container')

    def __init__(self, **kwargs: Dict[str, Any]) -> None:
        super().__init__(**kwargs)

        self._sidebar = Sidebar()
        # Gtk.Paned doesn't have a way to set the position of children in the ui file...
        child1 = self.main_paned.get_child1()
        self.main_paned.remove(child1)
        self.main_paned.pack1(self._sidebar, False, False)
        self._sidebar.connect('notify::location',
                              lambda obj, pspec: self.load_location(self._sidebar.location))
        default_location = Gio.File.new_for_path(DEFAULT_PATH)
        self._view_controller = ViewController(default_location)
        self._view_icon = IconView(self._view_controller)
        self._view_list = ListView2(self._view_controller)
        self.view_icon_container.add(self._view_icon)
        self.view_list_container.add(self._view_list)

        self._view_controller.connect("notify::location", self._view_controller_on_location_changed)
        load_location_action = Gio.SimpleAction.new("load_location", None)
        load_location_action.connect("activate", self._on_load_location)
        self.add_action(load_location_action)

        self.main_stack.set_visible_child_name('icon')
        self._path_bar = PathBar()
        self.path_bar_container.add(self._path_bar.widget)
        self._sync_path_bar_with_location(default_location)

        self._sync_view()

    def _sync_location_with_path_entry(self) -> None:
        uri = self._path_bar.location.get_uri()
        self._view_controller.location = Gio.File.new_for_uri(uri)

    def _sync_path_bar_with_location(self, location: Gio.File) -> None:
        self._path_bar.location = location

    def _view_controller_on_location_changed(self, _controller: ViewController,
                                             _location: Gio.File) -> None:
        self._sync_path_bar_with_location(self._view_controller.location)

    def _on_load_location(self, _action: Gio.Action, _value: None) -> None:
        self._sync_location_with_path_entry()

    def load_location(self, location: Gio.File) -> None:
        self._sync_path_bar_with_location(location)
        self._sync_location_with_path_entry()

    def _sync_view(self) -> None:
        if self.main_stack.get_visible_child_name() == 'list':
            icon = Gtk.Image.new_from_gicon(Gio.Icon.new_for_string('view-grid-symbolic'))
            self.view_switcher_button.add(icon)
        else:
            icon = Gtk.Image.new_from_gicon(Gio.Icon.new_for_string('view-list-symbolic'))
            self.view_switcher_button.add(icon)
        icon.show()

    @Gtk.Template.Callback('view_switcher_button_on_clicked')
    def _view_switcher_button_on_clicked(self, _button: Gtk.Button) -> None:
        for child in self.view_switcher_button.get_children():
            self.view_switcher_button.remove(child)

        if self.main_stack.get_visible_child_name() == 'list':
            self.main_stack.set_visible_child_name('icon')
        else:
            self.main_stack.set_visible_child_name('list')
        self._sync_view()
