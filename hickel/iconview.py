# iconview.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from gi.repository import Gtk, GObject, Pango
from hickel.cachedfile import CachedFile
from hickel.containermaxwidth import ContainerMaxWidth
from hickel.viewcontroller import ViewController
from hickel import iconinfo


@Gtk.Template.from_resource('/org/gnome/Hickel/iconview.ui')
class IconView(Gtk.Bin):
    __gtype_name__='IconView'
    flow_box: Gtk.FlowBox = Gtk.Template.Child('flow_box')

    def __init__(self, controller: ViewController) -> None:
        super().__init__()
        self._controller = controller
        self._model = self._controller.model
        self.flow_box.bind_model(self._model, self._create_widget, None)

    @staticmethod
    def _create_widget(file_info: CachedFile,_user_data: None) -> Gtk.Widget:
        view_icon_item = IconViewItem()
        view_icon_item.file_info = file_info

        return view_icon_item

    @Gtk.Template.Callback('on_flow_box_child_activated')
    def _on_flow_box_child_activated(self, _flow_box: Gtk.FlowBox,
                                     child: Gtk.FlowBoxChild) -> None:
        if child.file_info.is_directory:
            self._controller.location = child.file_info.location
        else:
            print("TODO: Missing implementation of activation of files")


class IconViewItem(Gtk.FlowBoxChild):

    def __init__(self) -> None:
        super().__init__()
        builder = Gtk.Builder()
        builder.add_from_resource('/org/gnome/Hickel/fileitem.ui')
        self._container = builder.get_object('file_item')
        self._label = builder.get_object('label')
        self._icon = builder.get_object('icon')
        self._max_width_container = ContainerMaxWidth()
        self._max_width_container.max_width = iconinfo.VIEW_ICON_SIZE
        self._max_width_container.set_size_request(iconinfo.VIEW_ICON_SIZE, -1)
        self._max_width_container.add(self._container)
        self.add(self._max_width_container)

        self._file_info = None

    @GObject.Property(type=CachedFile)
    def file_info(self) -> CachedFile:
        return self._file_info

    @file_info.setter # type: ignore
    def file_info(self, value: CachedFile) -> None:
        self._file_info = value
        # We cannot fo to the regular background task way, since this needs to be handled by
        # gtk+ itself. load_icon_sync is not thread safe, as nothing in gtk+ is.
        self._label.set_text(self._file_info.display_name)
        iconinfo.load_icon(self._file_info, self._container,
                           self._icon, iconinfo.VIEW_ICON_SIZE)
