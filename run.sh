#!/usr/bin/env bash

export MANIFEST_PATH="org.gnome.Hickel.json"
export FLATPAK_MODULE="hickel"
export MESON_ARGS=""
export DBUS_ID="org.gnome.Hickel"

flatpak-builder --stop-at=${FLATPAK_MODULE} .app ${MANIFEST_PATH}
flatpak build .app meson --prefix=/app ${MESON_ARGS} .build
flatpak build .app ninja -C .build install
flatpak-builder --run .app ${MANIFEST_PATH} hickel
